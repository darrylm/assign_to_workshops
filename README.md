Imagine that you're putting on several arts and crafts workshops that will take
place all at the same time. You've got a lot of people coming to attend these 
workshops, and you want them all to leave happy.  Unfortunately you've got a
problem - more people signed up for the pottery turning workshop than you can
fit in the room. You'll have to bump some people to other classes, but many of 
those workshops are full or close to full as well. Tom listed basket weaving as 
his second choice workshop, but the class is already full. Should you bump someone
else from the class to accommodate Tom? What's the best way to give people the 
workshop closest to the top of their list?

This program answers that type of question. It models this type of problem using
cost flow modeling, a common type of graph problem. The program uses Google's 
cost flow solver. Google provides some [good examples](https://developers.google.com/optimization/assignment/assignment_min_cost_flow)
that demonstrate how to apply cost flow modeling to assignment problems. This is
another example along the same lines.

I wrote this program in response to a stack overflow question that is roughly the
workshop assignment situation described above. You can use the source code as a 
learning tool, an example of how to use flow modeling to solve assignment
problems.  Or, with a little tweaking, you can use it to solve real world
problems.  If you just want to solve against your own data, look in 
[assign_workshops.cs](assign_workshops.cs)
and replace the following code with your own list of workshops and people:

```csharp
        // Identifies all workshops, with the maximum number of people that can enroll in each.
        Dictionary<string, int> workshopCapacities = new Dictionary<string, int>()
        {
            {"A", 2 },
            {"B", 3 },
            {"C", 1 }
        };

        // Identifies each person by name, which maps to a list of workshops they want to attend in the order of preference
        Dictionary<string, string[]> peoplePreferences = new Dictionary<string, string[]>()
        {
            { "Betsy", new[]{ "A", "B", "C" } },
            { "Joe",   new[]{ "C", "B", "A" } },
            { "Maria", new[]{ "C", "A", "B" } },
            { "Kuang", new[]{ "C", "A", "B" } },
            { "Paula", new[]{ "B", "C", "A" } },
        };
```
The diagram below shows how this program models the workshop assignment problem.  Numbers in 
square braces show the initial supply, and numbers in parentheses show the arc capacity. Dig 
through the source code to learn more.

![Flow Network Model](doc/flow_network.png)
