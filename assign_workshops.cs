﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Google.OrTools.Graph;

class assign_workshops
{
    private static void SolveSampleProblem()
    {
        Console.WriteLine("Assignment Problem");

        // Identifies all workshops, with the maximum number of people that can enroll in each.
        Dictionary<string, int> workshopCapacities = new Dictionary<string, int>()
        {
            {"A", 2 },
            {"B", 3 },
            {"C", 1 }
        };

        // Identifies each person by name, which maps to a list of workshops they want to attend in the order of preference
        Dictionary<string, string[]> peoplePreferences = new Dictionary<string, string[]>()
        {
            { "Betsy", new[]{ "A", "B", "C" } },
            { "Joe",   new[]{ "C", "B", "A" } },
            { "Maria", new[]{ "C", "A", "B" } },
            { "Kuang", new[]{ "C", "A", "B" } },
            { "Paula", new[]{ "B", "C", "A" } },
        };

        var assignments = SolveAssignments(workshopCapacities, peoplePreferences, i => i * i);

        // Print the results
        foreach (string person in peoplePreferences.Keys)
        {
            string workshop = assignments[person];
            Console.WriteLine(string.Format("{0} attended {1}", person, workshop));
        }

        // Expected Values:
        //  * Betsy will attend A
        //  * Joe will attend B
        //  * Either Maria or Kuang will attend C.  The other will attend A.
        //    Which one is arbitrary.
        //  * Paula will attend B
    }

    public static Dictionary<string, string> SolveAssignments(
        Dictionary<string, int> workshopCapacities,
        Dictionary<string, string[]> peoplePreferences,
        Func<int, int> penaltyFunction)
    {
        int numPeople = peoplePreferences.Count;
        int numWorkshops = workshopCapacities.Count;

        // Give each person a 0-based index
        Dictionary<string, int> peopleIndices = peoplePreferences.
            Select((kvp, i) => Tuple.Create(kvp.Key, i)).
            ToDictionary(t => t.Item1, t => t.Item2);

        int startingWorkshopIndex = numPeople;

        // Give each workshop an index, starting at one higher than the
        // highest person index
        Dictionary<string, int> workshopIndices = workshopCapacities.
            Select((kvp, i) => Tuple.Create(kvp.Key, i+numPeople)).
            ToDictionary(t => t.Item1, t => t.Item2);

        // The sink index is one higher than the highest workshop index
        int sinkIndex = numPeople + numWorkshops;

        MinCostFlow solver = new MinCostFlow();

        // Create an arc from each person to each workshop.
        // Its capacity is 1 (the person can attend the workshop at most once),
        // and the cost is the penalty based on the priority order).
        // Also, track arc IDs so we can get extract results later.
        Dictionary<Tuple<string, string>, int> personWorkshopToArcIndex = new Dictionary<Tuple<string, string>, int>();
        foreach (var kvp in peoplePreferences)
        {
            string person = kvp.Key;
            string[] prefs = kvp.Value;
            int personIndex = peopleIndices[person];

            foreach (int i in Enumerable.Range(0, numWorkshops))
            {
                string workshop = prefs[i];
                int workshopIndex = workshopIndices[workshop];
                int cost = penaltyFunction(i);

                int arcIndex = solver.AddArcWithCapacityAndUnitCost(personIndex, workshopIndex, 1, cost);
                personWorkshopToArcIndex[Tuple.Create(person, workshop)] = arcIndex;
            }
        }

        // create an arc from each workshop to the sink.
        // Its capacity is the workshop capacity. Its cost is 0.
        foreach (var workshopAndIndex in workshopIndices)
        {
            string workshop = workshopAndIndex.Key;
            int workshopIndex = workshopAndIndex.Value;
            int workshopCapacity = workshopCapacities[workshop];

            solver.AddArcWithCapacityAndUnitCost(workshopIndex, sinkIndex, workshopCapacity, 0);
        }

        // Now add the node supplies.  They are 1 at each person (each person can
        // go to at most 1 workshop), and -numPeople at the sink (each person needs to
        // go to at least 1 workshop)
        foreach (int personIndex in peopleIndices.Values)
        {
            solver.SetNodeSupply(personIndex, 1);
        }
        solver.SetNodeSupply(sinkIndex, -numPeople);

        // Solve!
        solver.SolveMaxFlowWithMinCost();

        // Now extract results.
        // Loop through each arc from person to workshop.
        // If it's a 1, that's the workshop that person attended.
        Dictionary<string, string> assignments = new Dictionary<string, string>();
        foreach (var keyValuePair in personWorkshopToArcIndex)
        {
            int arcIndex = keyValuePair.Value;
            if (solver.Flow(arcIndex) == 1)
            {
                string person = keyValuePair.Key.Item1;
                string workshop = keyValuePair.Key.Item2;
                assignments.Add(person, workshop);
            }
        }
        return assignments;
    }

    static void Main()
    {
        SolveSampleProblem();
    }
}
